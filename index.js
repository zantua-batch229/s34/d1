// Default setup of express
// "require to load the module/package"

const express = require("express");

// creation of an app/project
//in layman's term, app is our server

const app = express();
const port = 3000;

// setup for allowing the server to handle data from requests
// middlewares

app.use(express.json()); // allowing system to handle api

// allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

// [SECTION] - routes

// GET method

app.get("/", (req,res) => {
	res.send("Hello world!");
});

app.post("/hello", (req,res) => {
	res.send(`Hello there ${req.body.firstname} ${req.body.lastname}!`);
});
// MOCK DATABASE 
// An array that will store user object

let users =[];
app.post("/signup", (req, res) => {
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successful`);

	} else	{
		res.send(`Please input BOTH username ans password`);
	}
});

// PUT/UPDATE method

app.put("/change-password", (req,res) => {

})

// homepage

app.get("/home", (req,res) => {
	res.send("Welcome to homepage!")
});

app.get("/users", (req,res) => {
	res.send(users);
});

app.delete("/deleteuser", (req,res) => {
	let message;
	if(users.length != 0){
		for(let i=0;i<users.length;i++){
			if(req.body.username == users[i].username){
				users.splice(users[i],1);
				message = `User ${req.body.username} has been deleted`;
				break;
			} else {
				message = `User not found`;
			}
		}
	}

	res.send(message);

});







app.listen(port,() => console.log(`Server is running at port ${port}`));

